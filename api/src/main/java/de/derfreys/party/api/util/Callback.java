package de.derfreys.party.api.util;

/**
 * Created by Robin on 11.07.2017.
 */
public interface Callback<T> {

    void done(T object);

}
