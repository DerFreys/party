package de.derfreys.party.api.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Robin on 11.07.2017.
 */
public class AtomicCallbackResolver<E> {

    private final AtomicBoolean isResolved = new AtomicBoolean(false);
    private final AtomicReference<E> object = new AtomicReference<>();
    private final List<Callback<E>> waiting = new CopyOnWriteArrayList<>();

    /**
     * Resolve the object to fire all callbacks which were added to the list
     * @param result The result of this class
     */
    public void resolve(E result) {
        object.set(result);

        isResolved.set(true);

        for (Callback<E> callback : waiting) {
            callback.done(result);
        }
        waiting.clear();
    }

    /**
     * Wait until the result is resolved
     * @param callback The callback which waits for the result
     */
    public void whenResolved(Callback<E> callback) {
        if (isResolved.get()) {
            callback.done(object.get());
            return;
        }
        waiting.add(callback);
    }

}
