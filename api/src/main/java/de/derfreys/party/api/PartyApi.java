package de.derfreys.party.api;

import de.derfreys.party.api.party.Party;
import de.derfreys.party.api.user.PartyUser;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Robin on 09.07.2017.
 */
public interface PartyApi {

    /**
     * Get the party of a user
     * @param partyUser partyUser object
     * @return Party object
     */
    Party getParty(PartyUser partyUser);

    /**
     * Get the partyUser by a proxiedPlayer object
     * @param player proxiedPlayer object
     * @return PartyUser object
     */
    PartyUser getPartyUser(ProxiedPlayer player);

    /**
     * Checks if this user is in a party
     * @param partyUser partyUser object you want to check
     * @return boolean true or false
     */
    boolean isInParty(PartyUser partyUser);

    /**
     * Create a new party
     * @param owner The owner of this party
     * @return The party object
     */
    Party createParty(PartyUser owner);

}
