package de.derfreys.party.api.user;

import de.derfreys.party.api.party.Party;
import de.derfreys.party.api.util.Callback;

/**
 * Created by Robin on 09.07.2017.
 */
public interface PartyUser {

    /**
     * Get the party of this user
     * @return partyUser object
     */
    Party getParty();

    /**
     * Toggle the party requests
     * @param acceptsRequest boolean if the user can receive party requests
     */
    void togglePartyRequest(boolean acceptsRequest);

    /**
     * Checks if this user can receive party requests
     * @return boolean true or false
     */
    boolean acceptsPartyRequest();

    /**
     * Checks if this user is the owner of the party he is in
     * @return boolean true or false
     */
    boolean isPartyOwner();

    /**
     * Set the last invite for this user
     * @param party The party he got the invite from
     */
    void setInvite(Party party);

    /**
     * Get the party which invited the user
     * @return Party object
     */
    Party getInvited();

    /**
     * The callback will be fired as soon as the setting of the user is loaded
     * @param callback Callback
     */
    void whenLoaded(Callback<Void> callback);

}
