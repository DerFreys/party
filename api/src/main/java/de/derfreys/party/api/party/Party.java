package de.derfreys.party.api.party;

import de.derfreys.party.api.user.PartyUser;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Set;

/**
 * Created by Robin on 09.07.2017.
 */
public interface Party {

    /**
     * Get the owner of this party
     * @return partyUser object
     */
    PartyUser getOwner();

    /**
     * Set the owner of this party
     * @param partyUser partyUser object
     */
    void setOwner(PartyUser partyUser);

    /**
     * Get all members of this party
     * @return List of partyUsers
     */
    Set<PartyUser> getMembers();

    /**
     * Add a partyUser to this party
     * @param partyUser partyUser object
     */
    void addPartyMember(PartyUser partyUser);

    /**
     * Remove a partyUser from this party
     * @param partyUser partyUser object
     */
    void removePartyMember(PartyUser partyUser);

    /**
     * Send all the players in this party to the given server
     * @param serverInfo serverInformation
     */
    void moveToServer(ServerInfo serverInfo);

    /**
     * Sends a message to all players in this party
     * @param baseComponents Multiple base components
     */
    void sendMessage(BaseComponent... baseComponents);

    /**
     * Delete this party
     */
    void disband();

    /**
     * Set a resource pack for all the members of this party
     * @param url The url of the resourcepack
     * @param hash A hash which is not required but can be set
     *             More at http://wiki.vg/Protocol#Resource_Pack_Send
     */
    void setResourcePack(String url, String hash);

}
