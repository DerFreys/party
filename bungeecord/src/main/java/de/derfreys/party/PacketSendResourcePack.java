package de.derfreys.party;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.*;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.connection.DownstreamBridge;
import net.md_5.bungee.protocol.AbstractPacketHandler;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.PacketWrapper;

import java.beans.ConstructorProperties;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;

/**
 * Created by Robin on 10.07.2017.
 */
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class PacketSendResourcePack extends DefinedPacket {

    private String url;
    private String hash;

    @ConstructorProperties({"url", "hash"})
    public PacketSendResourcePack(String url, String hash) {
        this.url = url;
        this.hash = hash != null ? hash.toLowerCase() : Hashing.sha1().hashString(url, Charsets.UTF_8).toString().toLowerCase();
    }

    @Override
    public void handle(AbstractPacketHandler handler) throws Exception {
        if(!(handler instanceof DownstreamBridge)) {
            return;
        }
        DownstreamBridge downstreamBridge = (DownstreamBridge) handler;
        Field connection = downstreamBridge.getClass().getField("con");
        connection.setAccessible(true);
        UserConnection userConnection = (UserConnection) connection.get(downstreamBridge);
        userConnection.getPendingConnection().handle(new PacketWrapper(this, Unpooled.copiedBuffer(ByteBuffer.allocate(Integer.toString(this.getUrl().length()).length()))));
    }

    public void read(ByteBuf buf) {
        this.url = readString(buf);
        try {
            this.hash = readString(buf);
        } catch (IndexOutOfBoundsException ex) {
            //Ignored
        }
    }

    public void write(ByteBuf buf) {
        writeString(this.url, buf);
        writeString(this.hash, buf);
    }

}
