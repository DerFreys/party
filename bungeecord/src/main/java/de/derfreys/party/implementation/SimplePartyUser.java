package de.derfreys.party.implementation;

import de.derfreys.party.PartySystem;
import de.derfreys.party.api.party.Party;
import de.derfreys.party.api.user.PartyUser;
import de.derfreys.party.api.util.AtomicCallbackResolver;
import de.derfreys.party.api.util.Callback;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Robin on 09.07.2017.
 */
@Getter
public class SimplePartyUser implements PartyUser {

    private final ProxiedPlayer player;
    private final PartySystem partySystem;

    private Party party;
    private boolean partyRequests = true;

    private Party invite;

    private AtomicCallbackResolver<Void> loadedSetting = new AtomicCallbackResolver<>();

    public SimplePartyUser(ProxiedPlayer player, PartySystem partySystem) {
        this.player = player;
        this.partySystem = partySystem;

        //We can do that async without getting any callback because the user can't type that fast
        partySystem.getProxy().getScheduler().runAsync(partySystem, new Runnable() {
            @Override
            public void run() {
                ResultSet resultSet = partySystem.getMySQL().query("SELECT count(*) FROM `party_requests` WHERE `uuid` = '" + player.getUniqueId().toString().replace("-", "") + "';");
                try {
                    if(resultSet.next()) {
                        int size = resultSet.getInt(1);
                        if(size != 0) {
                            partyRequests = false;
                        }
                    }
                    loadedSetting.resolve(null);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        if(party == null) {
            partySystem.removeFromPartyMap(this);
        }else{
            partySystem.setParty(this, party);
        }
        this.party = party;
    }

    @Override
    public void togglePartyRequest(boolean acceptsRequest) {
        this.partyRequests = acceptsRequest;
        if(acceptsRequest) {
            partySystem.getMySQL().update("DELETE FROM `party_requests` WHERE `uuid` = '" + player.getUniqueId().toString().replace("-", "") + "';");
        }else{
            partySystem.getMySQL().update("INSERT INTO `party_requests`(`uuid`) VALUES ('" + player.getUniqueId().toString().replace("-", "") + "');");
        }
    }

    @Override
    public boolean acceptsPartyRequest() {
        return partyRequests;
    }

    @Override
    public boolean isPartyOwner() {
        return party != null && party.getOwner().equals(this);
    }

    @Override
    public void setInvite(Party party) {
        this.invite = party;
    }

    @Override
    public Party getInvited() {
        return invite;
    }

    @Override
    public void whenLoaded(Callback<Void> callback) {
        loadedSetting.whenResolved(callback);
    }
}
