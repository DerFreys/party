package de.derfreys.party.implementation;

import de.derfreys.party.PacketSendResourcePack;
import de.derfreys.party.api.party.Party;
import de.derfreys.party.api.user.PartyUser;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Robin on 09.07.2017.
 */
public class SimpleParty implements Party {

    private PartyUser owner;
    private Set<PartyUser> members = new HashSet<>();

    public SimpleParty(PartyUser owner) {
        this.owner = owner;
        ((SimplePartyUser) owner).setParty(this);
    }

    @Override
    public PartyUser getOwner() {
        return owner;
    }

    @Override
    public void setOwner(PartyUser partyUser) {
        //We need to add the owner to the members first
        members.add(owner);
        this.owner = partyUser;
    }

    @Override
    public Set<PartyUser> getMembers() {
        return Collections.unmodifiableSet(members);
    }

    @Override
    public void addPartyMember(PartyUser partyUser) {
        members.add(partyUser);
        ((SimplePartyUser) partyUser).setParty(this);
    }

    @Override
    public void removePartyMember(PartyUser partyUser) {
        if(members.contains(partyUser)) {
            members.remove(partyUser);
            ((SimplePartyUser) partyUser).setParty(null);
        }
    }

    @Override
    public void moveToServer(ServerInfo serverInfo) {
        for(ProxiedPlayer player : getPlayers()) {
            player.connect(serverInfo);
        }
    }

    @Override
    public void sendMessage(BaseComponent... baseComponents) {
        for(ProxiedPlayer player : getPlayers()) {
            player.sendMessage(baseComponents);
        }
    }

    @Override
    public void disband() {
        for(PartyUser user : members) {
            ((SimplePartyUser) user).setParty(null);
        }

        ((SimplePartyUser) owner).setParty(null);

        members.clear();
        this.owner = null;
    }

    public void setResourcePack(String url, String hash) {
        for(ProxiedPlayer player : getPlayers()) {
            sendResourcePack(player, url, hash);
        }
    }

    private void sendResourcePack(ProxiedPlayer player, String url, String hash) {
        player.unsafe().sendPacket(new PacketSendResourcePack(url, hash));
    }

    /**
     * Get a set of proxiedPlayer objects
     * This set includes the owner
     * @return Set of proxiedPlayers
     */
    private Set<ProxiedPlayer> getPlayers() {
        Set<ProxiedPlayer> players = new HashSet<>();
        for(PartyUser partyUser : members) {
            ProxiedPlayer player = ((SimplePartyUser) partyUser).getPlayer();
            players.add(player);
        }
        players.add(((SimplePartyUser) owner).getPlayer());

        return players;
    }

}
