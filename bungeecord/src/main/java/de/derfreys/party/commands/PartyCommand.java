package de.derfreys.party.commands;

import de.derfreys.party.PartySystem;
import de.derfreys.party.api.party.Party;
import de.derfreys.party.api.user.PartyUser;
import de.derfreys.party.api.util.Callback;
import de.derfreys.party.implementation.SimplePartyUser;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 09.07.2017.
 */
public class PartyCommand extends Command {

    private PartySystem partySystem;
    private String prefix;

    public PartyCommand(PartySystem partySystem, String prefix) {
        super("party", "", "p");
        this.partySystem = partySystem;
        this.prefix = prefix;
    }

    public void execute(CommandSender sender, String[] args) {
        if(!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(new TextComponent("Nur für Spieler"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;
        PartyUser partyUser = partySystem.getPartyUser(player);
        if(args.length == 1) {
            switch (args[0]) {
                case "accept": {
                    if(partyUser.getParty() != null) {
                        player.sendMessage(new TextComponent(prefix + "§cDu bist bereits in einer Party"));
                        return;
                    }

                    Party party = partyUser.getInvited();
                    if(party == null) {
                        player.sendMessage(new TextComponent(prefix + "§cDu hast keine Einladung für eine Party bekommen"));
                        return;
                    }
                    partyUser.setInvite(null);
                    party.addPartyMember(partyUser);
                    party.sendMessage(new TextComponent(prefix + "§e" + player.getName() + " §7ist der Party beigetreten"));
                    party.setResourcePack("http://www.creeperrepo.net/direct/sphax/54ddfdd5b3df411d92d3a510704009aa/PureBDcraft%20%2064x%20MC112.zip", null);

                    break;
                }
                case "decline": {
                    if(partyUser.getParty() != null) {
                        player.sendMessage(new TextComponent(prefix + "§cDu bist bereits in einer Party"));
                        return;
                    }

                    Party party = partyUser.getInvited();
                    if(party == null) {
                        player.sendMessage(new TextComponent(prefix + "§cDu hast keine Einladung für eine Party bekommen"));
                        return;
                    }
                    partyUser.setInvite(null);
                    player.sendMessage(new TextComponent(prefix + "§7Du hast die Anfrage für die Party abbelehnt"));
                    ((SimplePartyUser) party.getOwner()).getPlayer().sendMessage(new TextComponent(prefix + "§e" + player.getName() + " §7hat die Anfrage abgelehnt"));
                    break;
                }
                case "leave": {
                    if(partyUser.getParty() == null) {
                        player.sendMessage(new TextComponent(prefix + "§cDu bist in keiner Party"));
                        return;
                    }

                    if(partyUser.isPartyOwner()) {
                        player.sendMessage(new TextComponent(prefix + "§cDu darfst die Party als Leader nicht verlassen"));
                        return;
                    }

                    partyUser.getParty().sendMessage(new TextComponent(prefix + "§e" + player.getName() + " §7hat die Party verlassen"));
                    partyUser.getParty().removePartyMember(partyUser);
                    break;
                }
                case "disband": {
                    if(partyUser.getParty() == null) {
                        player.sendMessage(new TextComponent(prefix + "§cDu bist in keiner Party"));
                        return;
                    }
                    if(!partyUser.isPartyOwner()) {
                        player.sendMessage(new TextComponent(prefix + "§cDu musst Eigentümer der Party sein"));
                        return;
                    }
                    partyUser.getParty().sendMessage(new TextComponent(prefix + "§cDie Party wurde aufgelöst"));
                    partyUser.getParty().disband();
                    break;
                }
                case "toggle": {
                    partyUser.togglePartyRequest(!partyUser.acceptsPartyRequest());

                    if(partyUser.acceptsPartyRequest()) {
                        player.sendMessage(new TextComponent(prefix + "§aDu nimmst nun Party-Anfragen an"));
                    }else{
                        player.sendMessage(new TextComponent(prefix + "§cDu nimmst nun keine Party-Anfragen mehr an"));
                    }
                    break;
                }
                default: {
                    sendHelpMessage(player);
                }
            }
        }else if(args.length == 2) {
            if(args[0].equalsIgnoreCase("invite")) {
                if(partyUser.getParty() != null && !partyUser.isPartyOwner()) {
                    player.sendMessage(new TextComponent(prefix + "§cDu musst Eigentümer der Party sein"));
                    return;
                }

                if(args[1].equalsIgnoreCase(player.getName())) {
                    player.sendMessage(new TextComponent(prefix + "§cDu darfst dich nicht selber einladen"));
                    return;
                }

                ProxiedPlayer memberPlayer = partySystem.getProxy().getPlayer(args[1]);
                if(memberPlayer == null || !memberPlayer.isConnected()) {
                    player.sendMessage(new TextComponent(prefix + "§cDer Spieler §e" + args[1] + " §cwurde nicht gefunden"));
                    return;
                }

                PartyUser member = partySystem.getPartyUser(memberPlayer);
                member.whenLoaded(new Callback<Void>() {
                    @Override
                    public void done(Void object) {
                        if(!member.acceptsPartyRequest()) {
                            player.sendMessage(new TextComponent(prefix + "§cDieser Spieler nimmt keine Party-Anfragen an"));
                            return;
                        }

                        Party party = partyUser.getParty() == null ? partySystem.createParty(partyUser) : partyUser.getParty();
                        member.setInvite(party);

                        player.sendMessage(new TextComponent(prefix + "§7Du hast §e" + memberPlayer.getName() + " §7in die Party eingeladen"));
                        memberPlayer.sendMessage(new TextComponent(prefix + "§7Du hast eine Einladung in die Party von §e" + player.getName() + " §7bekommen"));

                        TextComponent accept = new TextComponent("§aANNEHMEN ");
                        accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/party accept"));

                        TextComponent decline = new TextComponent("§cABLEHNEN");
                        decline.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/party decline"));

                        memberPlayer.sendMessage(accept, decline);
                    }
                });
            }else if(args[0].equalsIgnoreCase("owner")) {
                if(partyUser.getParty() == null) {
                    player.sendMessage(new TextComponent(prefix + "§cDu bist in keiner Party"));
                    return;
                }
                if(!partyUser.isPartyOwner()) {
                    player.sendMessage(new TextComponent(prefix + "§cDu musst Eigentümer der Party sein"));
                    return;
                }

                ProxiedPlayer memberPlayer = partySystem.getProxy().getPlayer(args[1]);
                if(memberPlayer == null || !memberPlayer.isConnected()) {
                    player.sendMessage(new TextComponent(prefix + "§cDer Spieler §e" + args[1] + " §cwurde nicht gefunden"));
                    return;
                }

                PartyUser member = partySystem.getPartyUser(memberPlayer);
                partyUser.getParty().setOwner(member);
                partyUser.getParty().sendMessage(new TextComponent(prefix + "§e" + memberPlayer.getName() + " §7ist der neue Inhaber der Party"));
            }else{
                sendHelpMessage(player);
            }
        }else{
            sendHelpMessage(player);
        }
    }

    private void sendHelpMessage(ProxiedPlayer player) {
        player.sendMessage(new TextComponent(prefix + "§7PartySystem Command-Übersicht:"));
        player.sendMessage(new TextComponent("§7/p invite <Name> §eSende einem anderen Spieler eine Einladung"));
        player.sendMessage(new TextComponent("§7/p leave §eVerlasse deine Party"));
        player.sendMessage(new TextComponent("§7/p owner <Name> §eErnenne einen anderen Owner"));
        player.sendMessage(new TextComponent("§7/p disband §eLöse deine aktuelle Party auf"));
        player.sendMessage(new TextComponent("§7/p toggle §e(De)aktiviere Party-Anfragen"));
    }

}
