package de.derfreys.party.config;

import lombok.Getter;
import net.cubespace.Yamler.Config.Config;

import java.io.File;

/**
 * Created by Robin on 09.07.2017.
 */
@Getter
public class MainConfig extends Config {

    private String hostName = "localhost";
    private int port = 3306;
    private String user = "root";
    private String password = "password";
    private String database = "party";

    public MainConfig(File file) {
        CONFIG_FILE = file;
    }

}
