package de.derfreys.party;

import de.derfreys.party.api.PartyApi;
import de.derfreys.party.api.party.Party;
import de.derfreys.party.api.user.PartyUser;
import de.derfreys.party.commands.PartyCommand;
import de.derfreys.party.config.MainConfig;
import de.derfreys.party.database.MySQL;
import de.derfreys.party.listener.LoginListener;
import de.derfreys.party.listener.QuitListener;
import de.derfreys.party.implementation.SimpleParty;
import de.derfreys.party.implementation.SimplePartyUser;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.protocol.Protocol;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by Robin on 09.07.2017.
 */
public class PartySystem extends Plugin implements PartyApi {

    @Getter private static PartyApi partyApi;

    private Map<PartyUser, Party> partyMap = new HashMap<>();
    private Map<ProxiedPlayer, PartyUser> userMap = new HashMap<>();

    @Getter private MySQL mySQL;

    @Override
    public void onEnable() {
        //Init api
        partyApi = this;

        //Register implementation command
        getProxy().getPluginManager().registerCommand(this, new PartyCommand(this, "§7[§eParty§7] "));

        //Init the config
        MainConfig mainConfig = new MainConfig(new File(getDataFolder(), "config.yml"));

        try {
            mainConfig.init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        //Create mysql instance and connect
        mySQL = new MySQL(mainConfig.getHostName(), mainConfig.getPort(), mainConfig.getUser(), mainConfig.getPassword(), mainConfig.getDatabase());
        mySQL.connect();

        if(mySQL.isConnected()) {
            getLogger().info("Successfully connected to MySQL-Database!");
        }else{
            getLogger().warning("Couldn't connect to MySQL-Database! Please enter the right data");
        }

        //Create the table we need
        mySQL.update("CREATE TABLE IF NOT EXISTS `party_requests`(`uuid` VARCHAR(32), PRIMARY KEY(uuid));");

        //Try to register the resource packet
        try {
            registerResourcePacket();
        } catch (Exception e) {
            getLogger().warning("Couldn't init resource packet!");
            e.printStackTrace();
        }

        //Register events
        getProxy().getPluginManager().registerListener(this, new LoginListener(this));
        getProxy().getPluginManager().registerListener(this, new QuitListener(this, "§7[§eParty§7] "));
    }

    @Override
    public void onDisable() {
        mySQL.close();
    }

    public Party getParty(PartyUser partyUser) {
        return partyMap.get(partyUser);
    }

    public PartyUser getPartyUser(ProxiedPlayer player) {
        return userMap.get(player);
    }

    @Override
    public boolean isInParty(PartyUser partyUser) {
        return partyMap.containsKey(partyUser);
    }

    @Override
    public Party createParty(PartyUser owner) {
        SimpleParty party = new SimpleParty(owner);
        partyMap.put(owner, party);
        return party;
    }

    public void addPartyUser(ProxiedPlayer player) {
        userMap.put(player, new SimplePartyUser(player, this));
    }

    public void removePartyUser(ProxiedPlayer player) {
        userMap.remove(player);
    }

    public void setParty(PartyUser partyUser, Party party) {
        partyMap.put(partyUser, party);
    }

    public void removeFromPartyMap(PartyUser partyUser) {
        partyMap.remove(partyUser);
    }

    private void registerResourcePacket() throws Exception {
        Method map = Protocol.class.getDeclaredMethod("map", int.class, int.class);
        map.setAccessible(true);
        Object[] mappings = {
                map.invoke(null, ProtocolConstants.MINECRAFT_1_8, 0x48),
                map.invoke(null, ProtocolConstants.MINECRAFT_1_9, 0x32),
                map.invoke(null, ProtocolConstants.MINECRAFT_1_12, 0x33)
        };
        Object mappingsObject = Array.newInstance(mappings[0].getClass(), mappings.length);
        for (int i = 0; i < mappings.length; i++) {
            Array.set(mappingsObject, i, mappings[i]);
        }
        Object[] mappingsArray = (Object[]) mappingsObject;
        Method reg = Protocol.DirectionData.class.getDeclaredMethod("registerPacket", Class.class, mappingsArray.getClass());
        reg.setAccessible(true);
        reg.invoke(Protocol.GAME.TO_CLIENT, PacketSendResourcePack.class, mappingsArray);
    }

}
