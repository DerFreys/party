package de.derfreys.party.listener;

import de.derfreys.party.PartySystem;
import de.derfreys.party.api.user.PartyUser;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Robin on 09.07.2017.
 */
@AllArgsConstructor
public class QuitListener implements Listener {

    private PartySystem partySystem;
    private String prefix;

    @EventHandler
    public void onQuit(PlayerDisconnectEvent event) {
        ProxiedPlayer player = event.getPlayer();
        PartyUser partyUser = partySystem.getPartyUser(player);
        if(partyUser.getParty() != null) {
            if(partyUser.isPartyOwner()) {
                partyUser.getParty().sendMessage(new TextComponent(prefix + "§cDie Party wurde aufgelöst"));
                partyUser.getParty().disband();
            }else{
                partyUser.getParty().sendMessage(new TextComponent(prefix + "§e" + player.getName() + " §7hat die Party verlassen"));
                partyUser.getParty().removePartyMember(partyUser);
            }
        }

        partySystem.removePartyUser(player);
    }

}
