package de.derfreys.party.listener;

import de.derfreys.party.PartySystem;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Robin on 09.07.2017.
 */
@AllArgsConstructor
public class LoginListener implements Listener {

    private PartySystem partySystem;

    @EventHandler
    public void onLogin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        partySystem.addPartyUser(player);
    }

}
