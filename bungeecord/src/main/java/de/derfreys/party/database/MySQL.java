package de.derfreys.party.database;

import lombok.RequiredArgsConstructor;

import java.sql.*;

/**
 * Created by Robin on 09.07.2017.
 */
@RequiredArgsConstructor
public class MySQL{

    private final String hostName;
    private final int port;
    private final String user;
    private final String password;
    private final String database;

    private Connection connection;

    public Connection connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.hostName + ":" + this.port + "/" + this.database, this.user, this.password);
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return connection;
    }

    public boolean isConnected() {
        try {
            return !(this.connection == null || !this.connection.isValid(10) || this.connection.isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Connection getConnection() {
        return connection;
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                connection = null;
            }
        }
    }

    public ResultSet query(String query) {
        Connection connection = isConnected() ? getConnection() : connect();

        try {
            return connection.prepareStatement(query).executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void update(String update) {
        Connection connection = isConnected() ? getConnection() : connect();

        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(update);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
